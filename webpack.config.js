const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

module.exports = {
  output: {
    uniqueName: "shell"
  },
  optimization: {
    // Only needed to bypass a temporary bug
    runtimeChunk: false
  },
  plugins: [
    new ModuleFederationPlugin({
      remotes: {
        mfeLinkNav: "mfeLinkNav@http://localhost:5678/remoteEntry.js",
        mfeHeader: "mfeHeader@http://localhost:1234/remoteEntry.js",
      },
      shared: {
        "@angular/core": { singleton: true },
        "@angular/common": { singleton: true },
        "@angular/router": { singleton: true },
        "rxjs": { singleton: true },
        "@ngrx/effects": { singleton: true },
        "@ngrx/router-store": { singleton: true },
        "@ngrx/store": { singleton: true },
        "medline-mfe-store": { singleton: true }
      }
    }),
  ],
};
