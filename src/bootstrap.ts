import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { loadRemoteEntry } from '@angular-architects/module-federation';

if (environment.production) {
  enableProdMode();
}

loadRemoteEntry('http://localhost:3000/remoteEntry.js', 'mfe1');
loadRemoteEntry('http://localhost:3001/remoteEntry.js', 'mfe2');

// Prior to running the main app, prefetch all remote resources
Promise.all([
  loadRemoteEntry('http://localhost:1234/remoteEntry.js', 'mfeHeader'),
  loadRemoteEntry('http://localhost:5678/remoteEntry.js', 'mfeLinkNav')
]).then(values => {
    platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));
  }
);

