import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { APP_ROUTES } from './app.routes';
import { ConfigComponent } from './config/config.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AppFeature as fromApp } from 'medline-mfe-store/projects/medline-mfe-store/src/public-api';
import { NotificationEffects } from 'medline-mfe-store/projects/medline-mfe-store/src/lib/notification.effects';

//import * as fromApp  from './store/app/app';

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(APP_ROUTES),

    StoreModule.forFeature(fromApp.FEATURE_KEY, fromApp.appReducer),
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),

    /*
    EffectsModule.forFeature([MyEffects]),
    StoreModule.forFeature(myFeatureKey, myReducer),
    */
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    ConfigComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
