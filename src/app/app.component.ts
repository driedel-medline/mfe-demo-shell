import { EventEmitter, OnDestroy, ViewChild } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { Component, ComponentFactoryResolver, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

//import * as fromApp  from './store/app/app';
//import * as fromNotification from './store/app/notification';
import { AppFeature as fromApp, NotificationFeature as fromNotification } from 'medline-mfe-store/projects/medline-mfe-store/src/public-api';
import { Observable, Subject } from 'rxjs';
import { buildRoutes } from '../menu-utils';
import { LookupService } from './microfrontends/lookup.service';
import { Microfrontend } from './microfrontends/microfrontend';

interface IMFEHeaderComponent
{
  item: string;
  notification: any;
  headerEvent: EventEmitter<string>;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
  // Placeholder for microfront end header
  @ViewChild('mfeHeader', {read: ViewContainerRef})
  headerViewContainerRef: ViewContainerRef;

  // Placeholder for microfront end link navigation
  @ViewChild('mfeLinkNav', {read: ViewContainerRef})
  linkViewContainerRef: ViewContainerRef;


  // Reference to finalized microfrontend header
  headerCompInstance: IMFEHeaderComponent;
  navCompInstance;

  microfrontends: Microfrontend[] = [];
  eventCaught: boolean = false;

  notifications$: Observable<fromNotification.Notification[]>;
  notificationToNav$;
  destroyed$ = new Subject();

  constructor(
    private router: Router,
    private lookupService: LookupService,
    private cfr: ComponentFactoryResolver,
    private store: Store<fromApp.AppState>) {
  }

  async ngOnInit(): Promise<void> {
    // Load remote-located route resources
    this.microfrontends = await this.lookupService.lookup();
    const routes = buildRoutes(this.microfrontends);
    this.router.resetConfig(routes);

     // Bind component notification observable to store's data
     this.notifications$ = this.store.select(fromApp.selectNotificationMessages);
  }

  navigateTo(event)
  {
    this.router.navigate([`/${event.detail.href}`]);
  }

  async ngAfterViewInit()
  {
    this.headerViewContainerRef.clear();

    // Lazy load micro frontend components
    import('mfeHeader/MFEHeaderComponent').then(
      ({ MFEHeaderComponent }) => {
        const headerComp = this.headerViewContainerRef.createComponent(
          this.cfr.resolveComponentFactory(MFEHeaderComponent)
        );


        if(headerComp && headerComp.instance)
        {
          this.headerCompInstance = headerComp.instance as IMFEHeaderComponent;

          // Listen to changes on notification observable and do something
          this.notifications$.subscribe((items) => {
            this.headerCompInstance.notification = items;
            console.log(items, 'items to head')
          });

           // Pass sample data into header component
          this.headerCompInstance.item = "Data Passed from Shell";
        }
      }
    );


    import('mfeLinkNav/MFELinkNavComponent').then(
      ({ MFELinkNavComponent }) => {
        const navComp = this.linkViewContainerRef.createComponent(
          this.cfr.resolveComponentFactory(MFELinkNavComponent)
        );

        if(navComp && navComp.instance) {
          this.navCompInstance = navComp.instance;

          this.notifications$.subscribe((items) => {
            this.navCompInstance.notification = items;
            console.log(items, 'items to nav')
          });

          this.navCompInstance.addNotifEvent.subscribe(data => {
            this.store.dispatch(fromNotification.addItem(data))
          });

          this.navCompInstance.deleteNotifEvent.subscribe(data => {
            console.log(data, 'data from delete notification')
            this.store.dispatch(fromNotification.deleteItem(data))
          });
        }

      }
    );
  }


  clearNotifications()
  {
    this.store.dispatch(fromNotification.deleteAllItems());
  }


  ngOnDestroy()
  {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}

